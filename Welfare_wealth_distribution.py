#%% Packages

import numpy as np

import pandas as pd

#%% Importação de dados

data= pd.read_csv('Simulated_data_3_ITA_20G.txt', delimiter = r'\s+', header=None, names=['Time','Age','ID Number', 'Weight', 'Wealth', 'Consumption', 'Wage', 'Hours Worked', 'Earnings', 'Labor Income Tax', 'Social Welfare'])


#%%
data_1 = data.loc[data['Time']==1]

data_1_negative = data_1.loc[data_1['Wealth']<0]

data_1_zero = data_1.loc[data_1['Wealth']==0]

data_1_positive = data_1.loc[data_1['Wealth']>0]

quantiles_1=[]

segment_1 = []

data_wealth_1 = data_1['Wealth']

data_age_1 = data_1['Age']

quantiles_1.append(data_wealth_1.quantile(0))

quantiles_age=[]

segment_age=[]

quantiles_age.append(data_age_1.quantile(0))

mean_welfare_1=[]

mean_wealth=[]


for i in range(1,6):
    
    quantiles_1.append(data_wealth_1.quantile(i/5))
    
    segment_1.append(data_1.loc[(data_1['Wealth']<quantiles_1[i]) & (data_1['Wealth']>quantiles_1[i-1])])
    
    mean_welfare_1.append(segment_1[i-1]['Social Welfare'].mean())

    quantiles_age.append(data_age_1.quantile(i/5))
    
    segment_age.append(data_1.loc[(data_1['Age']<quantiles_age[i]) & (data_1['Age']>quantiles_age[i-1])])

    mean_wealth.append(segment_age[i-1]['Wealth'].mean())

    
    
    
    
    

data_1.describe()

data_1_poor = data_1.loc[data_1['Wealth']==-0.09]

data_1_poor.describe()

Welfare_old = data_1.loc[data_1['Age']>=65]['Social Welfare'].mean()

Welfare_young = data_1.loc[data_1['Age']<65]['Social Welfare'].mean()
